/*
 * @Author: shihong.lei
 * @Date: 2018-10-31 10:59:02
 * @Last Modified by: shihong.lei
 * @Last Modified time: 2018-11-15 14:32:08
 */
let globals = {
  mockEnv: 'test',
  version: '1.0', // 版本号
  unifiedLogin: '',
  host: 'http://10.16.84.144:8080/', // 接口地址
  domain: '',
  hostServe: {
    test: 'http://10.16.84.144:8080/'
  },
  domainServe: {
    test: '.yoai.com'
  },
  cookieProp: { path: '/' },
  Reg: {
    verifyPass: /\w{6,20}/,
    verifyAddUserPass: /^[a-zA-Z0-9]{6,20}$/,
    verifyLoginName: /\w{4,20}/,
    verifyMSM: /^\d{6}$/,
    verifyNOChinese: /[^\u4e00-\u9fa5]+/,
    verifyMobile: /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/,
    verifyPhone: /(^(\d{3}-)?\d{8})$|(1[0-9]{10})/,
    verifyIdCard: /^(\d{18,18}|\d{15,15}|\d{17,17}[x,X])$/,
    verifyRoom: /^[0-9a-zA-Z]$/,
    verifyByName: /[a-zA-Z\d\u4e00-\u9fa5]/
  },
  init () {
    let nowHostValue = window.location.hostname
    if (nowHostValue.indexOf('10') > -1) {
      globals.host = 'http://10.16.84.144:8080/'
      globals.domain = ''
    } else if (nowHostValue === 'localhost' || nowHostValue.indexOf('192.168.') > -1 || nowHostValue.indexOf('172.16.') > -1 || nowHostValue.indexOf('127.0.') > -1) {
      let mockEnv = globals.mockEnv
      globals.host = globals.hostServe[mockEnv]
      console.log(globals.host)
      globals.domain = ''
    }
  }
}

export default globals
