/*
 * @Author: shihong.lei
 * @Date: 2018-11-15 14:31:37
 * @Last Modified by:   shihong.lei
 * @Last Modified time: 2018-11-15 14:31:37
 */

import axios from './http.js'
import globals from './config'
export const getData = (url = '', data = {}, type = 'GET') => {
  type = type.toUpperCase()
  if (type === 'GET') {
    let dataStr = ''
    Object.keys(data).forEach(key => {
      dataStr += key + '=' + data[key] + '&'
    })
    if (dataStr !== '') {
      dataStr = dataStr.substr(0, dataStr.lastIndexOf('&'))
      url = url + '?' + encodeURI(dataStr) + '&t=' + new Date().getTime()
    }
  }
  if (url.indexOf('?') < 0) {
    url = encodeURI(url) + '?t=' + new Date().getTime()
  }
  // console.log(url)
  let ROOT = process.env.NODE_ENV === 'development' ? '/api' : globals.host
  return axios({
    method: type,
    baseURL: ROOT,
    url: url,
    data: data
  })
}
