/*
 * @Author: shihong.lei
 * @Date: 2018-11-15 14:30:45
 * @Last Modified by: shihong.lei
 * @Last Modified time: 2018-11-15 14:45:15
 */

import Vue from 'vue'
import axios from 'axios'
import Cookies from 'js-cookie'
import { Loading, Message } from 'element-ui'

// axios 配置
axios.defaults.timeout = 10000
axios.defaults.withCredentials = true
let loading

// axios.defaults.baseURL = globals.host
// http request 拦截器
axios.interceptors.request.use(
  config => {
    loading = Loading.service({ text: '拼命加载中' })
    return config
  },
  err => {
    return Promise.reject(err)
  })

// http response 拦截器
axios.interceptors.response.use(
  response => {
    if (loading) {
      setTimeout(function () {
        Vue.nextTick(() => {
          loading.close()
        })
      }, 0)
    }
    if (response.data) {
      if (parseInt(response.data.code) === 4) {
        Cookies.set('status', false)
      } else if (parseInt(response.data.code) === 2) {
        if (response.data.message && response.data.message.length > 0) {
          Message.error(response.data.message)
        } else {
          Message.error('服务器错误，请您稍后再试')
        }
      }
    } else {
      Message.error('服务器错误，请您稍后再试')
    }
    return response
  },
  error => {
    if (loading) {
      Vue.nextTick(() => {
        loading.close()
      })
    }
    Message.error('服务器错误，请您稍后再试')
    return Promise.reject(error)
  })

export default axios
